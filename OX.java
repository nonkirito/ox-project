
import java.io.IOException;
import java.util.Scanner;

public class OX {

    public static void main(String[] args) throws IOException {
        String table[][] = new String[3][3];
        String player = "O";
        int countround = 0;
        int countlike = 0;
        createTable(table);
        showWelcome();
        while (true) {
            showTable(table);
            showPlayerTurn(player);
            if (inputRowCol(table, player)) {
                countround++;
            }
            if (checkResult(countlike, countround, table, player)) {
                showFinish();
                break;
            }
            player = switchPlayerTurn(player);
        }
    }

    public static void createTable(String table[][]) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = "-";
            }
        }
    }

    public static void showWelcome() {
        System.out.println(" ------------------\n" + "|Welcome to OX Game|\n" + " ------------------");
    }

    public static void showTable(String table[][]) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
                if (j == 2) {
                    System.out.println();
                }
            }
        }
    }

    public static void showPlayerTurn(String player) {
        System.out.println("--- " + player + " Turn ---");
    }

    public static boolean inputRowCol(String table[][], String player) throws IOException {
        Scanner scan = new Scanner(System.in);
        boolean checkloopinput = true;
        while (checkloopinput) {
            try {
                showInputRowCol();         
                int inputrow = scan.nextInt();
                int inputcol = scan.nextInt();
                if (checkInput(inputrow, inputcol, player, table)) {
                    checkloopinput = false;
                    return true;
                }
            } catch (Exception e) {
                showInputErrorNotNumber();
                scan.nextLine();
                continue;
            }
        }
        return false;
    }

    public static void showInputRowCol() {
        System.out.println("Please input Row and Column: ");
    }

    public static void showInputErrorNotNumber() {
        System.out.println("Please input number only!");
    }

    public static void showNotInputInRange() {
        System.out.println("Please input again. Input is out of range.");
    }

    public static void showInputErrorRepeat() {
        System.out.println("Pleae input again. This column is not available.");
    }

    public static boolean checkInput(int inputrow, int inputcol, String player, String table[][]) {
        if (inputrow > 3 || inputcol > 3) {
            showNotInputInRange();
            return false;
        } else if (table[inputrow - 1][inputcol - 1].equals("-")) {
            table[inputrow - 1][inputcol - 1] = player;
            return true;
        } else {
            showInputErrorRepeat();
            return false;
        }
    }

    public static String switchPlayerTurn(String player) {
        if (player.equals("O")) {
            player = "X";
            return player;
        } else if (player.equals("X")) {
            player = "O";
            return player;
        }
        return "";
    }

    public static boolean checkResult(int countlike, int countround, String table[][], String player) {
        if (checkHorizontal(countlike, table, player)) {
            return true;
        } else if (checkVertical(countlike, table, player)) {
            return true;
        } else if (checkDiagonal(countlike, table, player)) {
            return true;
        } else if (checkDraw(countround, table)) {
            return true;
        }
        return false;

    }

    public static boolean checkHorizontal(int countlike, String table[][], String player) {
        if (checkHorizontal1(countlike, table, player)) {
            return true;
        } else if (checkHorizontal2(countlike, table, player)) {
            return true;
        } else if (checkHorizontal3(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical(int countlike, String table[][], String player) {
        if (checkVertical1(countlike, table, player)) {
            return true;
        } else if (checkVertical2(countlike, table, player)) {
            return true;
        } else if (checkVertical3(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkDiagonal(int countlike, String table[][], String player) {
        if (checkDiagonal1(countlike, table, player)) {
            return true;
        } else if (checkDiagonal2(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkHorizontal1(int countlike, String table[][], String player) {
        countlike = 0;
        for (int i = 0; i < 3; i++) {
            if (table[0][i].equals(player)) {
                countlike++;
            }
        }
        if (checkWin(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkHorizontal2(int countlike, String table[][], String player) {
        countlike = 0;
        for (int i = 0; i < 3; i++) {
            if (table[1][i].equals(player)) {
                countlike++;
            }
        }
        if (checkWin(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkHorizontal3(int countlike, String table[][], String player) {
        countlike = 0;
        for (int i = 0; i < 3; i++) {
            if (table[2][i].equals(player)) {
                countlike++;
            }
        }
        if (checkWin(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical1(int countlike, String table[][], String player) {
        countlike = 0;
        for (int i = 0; i < 3; i++) {
            if (table[i][0].equals(player)) {
                countlike++;
            }
        }
        if (checkWin(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical2(int countlike, String table[][], String player) {
        countlike = 0;
        for (int i = 0; i < 3; i++) {
            if (table[i][1].equals(player)) {
                countlike++;
            }
        }
        if (checkWin(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical3(int countlike, String table[][], String player) {
        countlike = 0;
        for (int i = 0; i < 3; i++) {
            if (table[i][2].equals(player)) {
                countlike++;
            }
        }
        if (checkWin(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkDiagonal1(int countlike, String table[][], String player) {
        countlike = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i == j) {
                    if (table[i][j].equals(player)) {
                        countlike++;
                    }
                }
            }
        }
        if (checkWin(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkDiagonal2(int countlike, String table[][], String player) {
        countlike = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 2; j >= 0; j--) {
                if (i - j == 2 || j - i == 2) {
                    if (table[i][j].equals(player)) {
                        countlike++;
                    }
                }
                if (i == 1 && j == 1) {
                    if (table[i][j].equals(player)) {
                        countlike++;
                    }
                }
            }
        }
        if (checkWin(countlike, table, player)) {
            return true;
        }
        return false;
    }

    public static boolean checkWin(int countlike, String table[][], String player) {
        if (countlike == 3) {
            showTable(table);
            showWin(player);
            return true;
        }
        return false;
    }

    public static void showWin(String player) {
        System.out.println("--- " + player + " Win! ---");
    }

    public static boolean checkDraw(int countround, String table[][]) {
        if (countround == 9) {
            showTable(table);
            showDraw();
            return true;
        }
        return false;
    }

    public static void showDraw() {
        System.out.println("--- Draw! ---\n" + "--- If table full and both sides cannot win OX game decision result is draw. ---");
    }

    public static void showFinish() {
        System.out.println("--- Game End. Bye Bye ---");
    }
}
